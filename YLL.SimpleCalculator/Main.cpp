
// Simple Calculator
// Yee Lor

#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool Divide(float num1, float num2, float& answer);
float Pow(float, int);

int main()
{
	string op;
	float num1;
	float num2;
	float answer1;

	while (op != "Q")
	{
		cout << "Enter in an operator: + - * / ^ or Q to Quit \n";
		cin >> op;
		if (op != "Q" && op != "q")
		{
			cout << "Number 1:  \n";
			cin >> num1;
			cout << "Number 2:  \n";
			cin >> num2;

			if (op == "+")
			{
				cout << num1 << " + " << num2 << " = " << Add(num1, num2) << "\n";
			}
			else if (op == "-")
			{
				cout << num1 << " - " << num2 << " = " << Subtract(num1, num2) << "\n";
			}
			else if (op == "*")
			{
				cout << num1 << " * " << num2 << " = " << Multiply(num1, num2) << "\n";
			}
			else if (op == "/")
			{
				if (Divide(num1, num2, answer1))
				{
					cout << num1 << " / " << num2 << " = " << num1 / num2 << "\n";
				}
				else
				{
					cout << "Cannot divide by zero." << "\n";
				}
			}
			else if (op == "^")
			{
				cout << num1 << " ^ " << num2 << " = " << pow(num1, num2) << "\n";
			}
			else
			{
				cout << "Please enter an operator." << "\n";
			}
		}
		else exit(EXIT_SUCCESS);		
	}

	_getch();
	return 0;
}

float Add(float num1, float num2)
{
	return num1 + num2;
}

float Subtract(float num1, float num2)
{
	return num1 - num2;
}
float Multiply(float num1, float num2)
{
	return num1 * num2;
}
bool Divide(float num1, float num2, float& answer)
{
	if (num2 == 0) return false;
	return true;
}
float Pow(float num1, int num2)
{
	if (num2 == 0) return 1;
	else return num1 * Pow(num1, num2 - 1);
}
